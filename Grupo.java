public class Grupo

{    
    public static void main(String[] args){
        
        FlavioJeronimo membroFlavio = new FlavioJeronimo();
        membroFlavio.nome = "Flávio";
        membroFlavio.sobrenome = " Jeronimo";

        /*------------------------------------------------------*/

        BernardoNebias membroBernardo = new BernardoNebias();
        membroBernardo.nome = "Bernardo";
        membroBernardo.sobrenome = "Halfeld";
        
        /*------------------------------------------------------*/
        
        PauloCarneiro membroPaulo = new PauloCarneiro();
        membroPaulo.nome="Paulo";
        membroPaulo.sobrenome = "Carneiro";
        
        /*------------------------------------------------------*/

        YugoOliveira membroYugo = new YugoOliveira();
        membroYugo.nome="Yugo";
        membroYugo.sobrenome = "Montalvao";
        
        /*------------------------------------------------------*/
        GustavoSantos membroGustavo = new GustavoSantos();
        membroGustavo.nome="Gustavo";
        membroGustavo.sobrenome = "Santos";
        
        /*------------------------------------------------------*/
        Membros.listaMembros(membroGustavo,membroFlavio,membroPaulo,membroYugo,membroBernardo);
        

    }
    
}
